package pingPong

import akka.actor.{Props, ActorRef, ActorSystem, Actor}

/**
 * Created by raitis on 17/03/15.
 */

case object PingMessage
case object PongMessage
case object StartMessage
case object StopMessage


class Pong extends Actor {
  override def receive = {
    case PingMessage =>
     println("Pong"); sender ! PongMessage
    case StopMessage =>
      println("Pong stopping"); context.stop(self)
    case _ => println("Unread Pong message")
  }
}


class Ping(pong: ActorRef) extends Actor {
  override def receive = {
    case StartMessage =>
      pingHim; pong ! PingMessage
    case PongMessage  =>
      pingHim
      if (count < 100)
        pong ! PingMessage
      else
        pong ! StopMessage
    case _ => println("Unread Ping message")
  }

  var count = 0

  def pingHim: Unit = {
    count += 1
    println("Ping")
  }
}


object Main extends App {

  val system = ActorSystem("PingPongSystem")
  // pong must be in front of ping because it is its variable
  val pong = system.actorOf(Props(new Pong()), name = "pongactor")
  val ping = system.actorOf(Props(new Ping(pong)), name = "pingactor")
  ping ! StartMessage

}