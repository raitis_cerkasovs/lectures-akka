package procesFile

import akka.actor.{ActorSystem, Actor, ActorRef, Props}

// from file processing
import scala.io.Source._

import akka.util.Timeout
import akka.pattern.ask
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global


/**
 * Created by raitis on 19/03/15.
 */
case class StartProcessingFile()
case class ProcessingLineMsg(line: String)
case class processingLineTotal(words: Int)


class FileOfWordsCounter(fileName: String) extends Actor {

  private var running = false

  // for specific output
  private var fileSender: Option[ActorRef] = None

  private var lineCounter = 0
  private var runningTotal = 0
  private var linesProcessed = 0


  override def receive = {


    // 2
    case StartProcessingFile =>
        if (running)
          println("already running")
        else {
          println ("Started running")
          running = true

          // ref to parrent process
          // for advance output
          fileSender = Some(sender)

          // open actor for each line
          fromFile(fileName).getLines().foreach {
            line => {
              context.actorOf(Props[WordsCounterActor], "wcactor" + lineCounter) ! ProcessingLineMsg(line)
              println(s"start actor " + "wcactor " + lineCounter)
              lineCounter += 1
            }
          }
        }


    // count total words
    // 5
    case processingLineTotal(words) =>
        println("line Processed " + words)
        runningTotal += words
        linesProcessed += 1
        if (linesProcessed == lineCounter) {
          // basic output
          println("finish, theres is " + runningTotal + " words" )
          // or better output:
          fileSender.map(_ ! runningTotal)
        }

    case _ =>
        println("wrong message")
  }
}


// count words in line and sends them to processingLineTotal
// 3
class WordsCounterActor extends Actor {
  override def receive = {
    case ProcessingLineMsg(line) =>
      println("Processing line")
      val wordsInLine = line.split(" ").length
      println(s"$wordsInLine words in this lane")

      // 4
      sender ! processingLineTotal(wordsInLine)

    case _ =>
      println("wrong message")
  }
}


object MainCounter extends App {

  val system = ActorSystem("wordsCounterSystem")
  val actor = system.actorOf(Props(new FileOfWordsCounter("src/text.txt")), "wcountactor")

  // basic start and output
  // actor ! StartProcessingFile

  // better start
  // 1
  implicit val timeout = Timeout(30 seconds)
  val future = actor ? StartProcessingFile
  future.map {
    result =>
      println("Total number of words: " + result)
      system.shutdown()
  }

}

 