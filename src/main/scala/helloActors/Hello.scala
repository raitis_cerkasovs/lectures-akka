package helloActors

import akka.actor.{Props, ActorSystem, Actor}

/**
 * Created by raitis on 16/03/15.
 */

class Hello(name: String) extends Actor {
   def receive = {
     case "hello" => println(s"Hello $name back to you")
     case _ => println("huh?")
   }
}

object Main extends App {

  val system = ActorSystem("HelloSystem")
  val helloActor = system.actorOf(Props(new Hello("Fred")), name = "helloactor")

  helloActor ! "hello"
  helloActor ! "buenos dias"

}
