package rayTracer

case class Ray(orig: Vector, dir: Vector)
