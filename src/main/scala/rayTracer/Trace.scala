package rayTracer

import javax.xml.bind.{ValidationEventLocator, ValidationEvent}

import akka.actor._
import com.sun.xml.internal.bind.v2.runtime.Coordinator

object Trace {

  val AntiAliasingFactor = 4
  val Width = 800
  val Height = 600

  var rayCount = 0
  var hitCount = 0
  var lightCount = 0
  var darkCount = 0

  // add actor system
  val system = ActorSystem("rayTracerSystem")
  val actor = system.actorOf(Props[CoordinatorActor], "startactor")

  def main(args: Array[String]): Unit = {

//    if (args.length != 2) {
//      println("usage: scala Trace input.dat output.png")
//      System.exit(-1)
//    }
//

    val (infile, outfile) = ("src/input.dat", "src/output.png")
    val scene = Scene.fromFile(infile)

    render(scene, outfile, Width, Height)

    println("Processing...")

  }




  def render(scene: Scene, outfile: String, width: Int, height: Int) = {

    var image: Image  = new Image(width, height)
    this.actor ! InitialiseMsg(scene, image, outfile, width, height)

 }


}