package rayTracer

import akka.actor.{ActorSystem, Actor, ActorRef, Props}

case class InitialiseMsg(scene: Scene, image: Image, outfile: String, width: Int, height: Int)
case class CoordinatorColorMsg(x: Int, y: Int, color: Colour)

object Coordinator {
  var waiting = 0
  var outfile: String = null
  var image: Image = null
}


class CoordinatorActor extends Actor {

  override def receive = {
    case InitialiseMsg(scene, image, outfile, width, height) => {
      Coordinator.waiting = image.width * image.height
      Coordinator.image = image
      Coordinator.outfile = outfile
      scene.traceImage(width, height)
    }
    case CoordinatorColorMsg(x, y, colour) => {
      Coordinator.image(x, y) = colour
      Coordinator.waiting -= 1
      if (Coordinator.waiting == 0) {
         Coordinator.image.print(Coordinator.outfile)

        println("rays cast " + Trace.rayCount)
        println("rays hit " + Trace.hitCount)
        println("light " + Trace.lightCount)
        println("dark " + Trace.darkCount)

        println("Finished.")

        Trace.system.shutdown()
      }
    }
    case _ => "Unrecognised message."

  }

}
